import { createActions } from "reduxsauce";

const { Types, Creators } = createActions({
    add: ["list"],
});

const Actions = {
    Types,
    Creators
};

export default Actions;