import React, {Fragment} from 'react';
import {Switch, Route } from "react-router";
import {Link} from 'react-router-dom'

const Menu = (props) => {
    const {menu} = props;
    const list = menu.map(item => {
        const {name, link} = item;

        return <div key={item.key}>
            <Link to={`/${link}`}>{name}</Link>
        </div>
    });
    const routers = menu.map(item => {
        const exact = item.key === 0;
        const {link} = item;

        return <Route exact={exact} path={`/${link}`}/>
    });

    return (
        <Fragment>
            <section>
                {list}
            </section>
            <Switch>
                {routers}
            </Switch>
        </Fragment>
    );
};

export default Menu;