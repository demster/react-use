import React from 'react';
import {connect} from 'react-redux';
import Menu from "../Menu";
import {HashRouter as Router} from 'react-router-dom'


const ShoppingList = (props) => {
    const {menu} = props;
    //const [list, setList] = useState(shoppingList);

    return (
        <Router>
            <Menu menu={menu}/>
        </Router>
    );
};

const mapStateToProps = () => {
    return {
        menu: [
            {
                name: 'Список покупок',
                link: 'shoppingList',
                key: 0
            },
            {
                name: 'Расходы',
                link: 'costs',
                key: 1
            }
        ],
    };
};

const mapDispatchToProps = () => {

};

export default connect(mapStateToProps, mapDispatchToProps)(ShoppingList);