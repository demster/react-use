import { fromJS } from "immutable";
import { createReducer } from "reduxsauce";
import Actions from "../actions/shopping-list-actions";

const INITIAL_STATE = fromJS({
    list: {},
});

const add = state => state.merge({list: state.toJS().list});

const Types = Actions.Types;

const reducer = createReducer(INITIAL_STATE, {
    [Types.ADD]: add,
});

export default reducer;